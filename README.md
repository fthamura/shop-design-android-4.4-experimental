﻿# README #
*Author: Pär Amsen, www.trixigt.com*

[*Project website presentation*](http://trixigt.com/android-uxui-shop-design/)
****

This project was created to advance my skills in Android and specifically Androids transition and animation frameworks. Furthermore I wanted to explore the somewhat briefly documented parts of how Android draw and lay out its graphics/visuals and how to manipulate the drawing process.

[Youtube project presentation](https://www.youtube.com/watch?v=jbclnZquthA)

I have also focused on creating a program structure as easy to understand for other developers as possible, focusing on DDD (Domain Driven Design) and OOD (Object Oriented Design) in the package/class structure.

The design of the visuals and animations are heavily inspired of a concept design made by Budo Tarim; [Dribbble design gif](https://dribbble.com/shots/1669228-Shop-360-interaction-app)


![GIF presentation animation](https://bitbucket.org/TriXigT/shop-design-android-4.4-experimental/downloads/shop_design_preview_img.gif)

## Downloads ##

[Version 0.5; Experimental packaged APK for testing purposes (no permissions in Android)](https://bitbucket.org/TriXigT/shop-design-android-4.4-experimental/downloads/ShopDesign_par_amsen_API_14-Andro_4.0.apk)

## Changelog ##

**Version 0.5:**
This version is highly experimental and does not always follow the Android standards (i.e. different versions of image graphics to support all screen sizes etc.). The program structure is created with the DDD standard in mind using a object/entity driven design and a separation of visual/business logic.

## Legal ##

This product is distributed under the [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).

## Credits/Thanks ##
**My course teacher Bertil Holmström** for being a great tutor in DDD and OOD (and also Java Enterprise Edition / overall Enterprise logic / everything Java and programming oriented)

**Designer Budo Tarim** who created the design this project is built around, I wouldn't have thought of the great aesthetics without his concept on Dribbble (design community).

## Contact ##
Pär Amsen,
www.trixigt.com,
trixigt@gmail.com